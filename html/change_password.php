<!DOCTYPE html>
<?php if(isset($_SESSION["username"])){
    header("location: index.php");
    exit;
} ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="styles.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <!-- <div class="fadeIn first">
                <img src="https://e1.pxfuel.com/desktop-wallpaper/130/54/desktop-wallpaper-png-pepsi-man-png-pepsiman.jpg" id="icon" alt="User Icon" />
            </div> -->

            <!-- Login Form -->
            
                <p><h3><strong> Change Password </strong></h3></p>
                <p>Please enter the password of your choice.</p>
                <input type="password" id="password" class="fadeIn second" name="password" placeholder="password" required>
                <input type="password" id="Cpassword" class="fadeIn third" name="Cpassword" placeholder="Confirm Password" required>
                <p id="result" style="display:none;"></p>
                <input id="submit" type="submit" class="fadeIn fourth" value="Log In">
        </div>
    </div>
</body>
<script>
    $(document).ready(function(){
        var resolution=screen.width+"x"+screen.height+"";
        $("#submit").on("click", function(){
            // Récupère la valeur de recherche
            var password = $("#password").val();
            var Cpassword = $("#Cpassword").val();
            // Vérifie si la longueur de la chaîne de recherche est supérieure à 2 caractères
            // Effectue une requête AJAX
            $.ajax({
                type: "POST",
                url: "data_treatment/change_password_treatment.php",
                data: {password:password,Cpassword:Cpassword },
                success: function(response){
                    // Affiche les résultats dans la div #searchResults
                    if(response=="Password changed ! Redirecting ..."){
                        setTimeout(function() {
                            window.location.href = "../startbootstrap-agency-gh-pages/index.php";
                        }, 1000);
                    $("#result").html(response);
                    $("#result").show();
                    }
                }
            });
        });
    });
    </script>
</html>