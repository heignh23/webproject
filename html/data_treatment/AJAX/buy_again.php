<?php
session_start();
include("../connect_database.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['orderNumber'])) {
    // Get the order details based on orderNumber
    $orderNumber = $_POST['orderNumber'];

    $sql = "SELECT * FROM orders WHERE orderNumber = '$orderNumber'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // Fetch order details
        $row = $result->fetch_assoc();

        // Decode and set the order items in $_SESSION["panier"]
        unset($_SESSION["panier"]);
        $_SESSION["panier"] = json_decode($row['items'], true);

        $_SESSION["promocode"] = $row['promocode'];

        $_SESSION["shipment"] = $row["shipment"];

        // Return success
        echo 'success';
        echo "Promocode: ".$row['promocode']." ".$_SESSION["promocode"];
    } else {
        // Return error if order not found
        echo 'Order not found.';
    }
} else {
    // Return error for invalid request
    echo 'Invalid request.';
}

$conn->close();
?>
