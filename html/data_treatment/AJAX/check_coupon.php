<?php

include("../connect_database.php");

// Récupérer le code promo envoyé par la requête AJAX
$receivedPromoCode = $_POST['promoCode'];

// Préparer la requête SQL
$sql = "SELECT discount FROM coupons WHERE couponCode = ?";

// Préparer et exécuter la déclaration
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $receivedPromoCode);
$stmt->execute();
$stmt->bind_result($discount);

// Vérifier si le code promo existe dans la base de données
if ($stmt->fetch()) {
    // Renvoyer la remise associée
    echo $discount;
} else {
    // Si le code promo n'est pas trouvé, renvoyer 0
    echo 0;
}

// Fermer la connexion
$stmt->close();
$conn->close();

?>