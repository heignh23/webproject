<?php
session_start();
include("../connect_database.php");

// Vérifier si l'article est déjà dans le panier
if (!isset($_SESSION['panier'])) {
    $_SESSION['panier'] = array();
}

$itemName = $_POST['itemName'];
$itemPrice = $_POST['itemPrice'];
$itemQuantity = $_POST['itemQuantity'];

// Vérifier si l'article est déjà dans le panier
if (array_key_exists($itemName, $_SESSION['panier'])) {
    // L'article est déjà dans le panier, mettez à jour la quantité
    $_SESSION['panier'][$itemName]['quantity'] += $itemQuantity;
} else {
    // L'article n'est pas encore dans le panier, ajoutez-le au panier et à la base de données
    $_SESSION['panier'][$itemName] = array(
        'price' => $itemPrice,
        'quantity' => $itemQuantity
    );
}

$response = array('success' => true, 'message' => 'Article ajouté au panier avec succès');
echo json_encode($response);
?>
