<?php
// online_users.php - Get count of online users with cleanup

include '../connect_database.php';

$timeout = 300; // 5 minutes
// Retrieve online user count
$sql = "SELECT COUNT(*) AS online_users FROM user_sessions WHERE last_activity > (NOW() - INTERVAL $timeout SECOND)";
$result = $conn->query($sql);

if ($result) {
    $row = $result->fetch_assoc();
    echo $row['online_users'];
} else {
    echo "0";
}

$conn->close();
?>
