<?php
session_start();
include("../connect_database.php");

$itemName = $_POST['itemName'];

// Vérifier si l'article est déjà dans le panier
if (array_key_exists($itemName, $_SESSION['panier'])) {
    // L'article est déjà dans le panier, mettez à jour la quantité
    unset($_SESSION['panier'][$itemName]);
}

$response = array('success' => true, 'message' => 'Article retiré au panier avec succès');
echo json_encode($response);
?>
