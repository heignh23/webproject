<?php
include("../connect_database.php");

if (isset($_GET['query'])) {
    $search_query = $_GET['query'];
    
    // Use the search query to fetch matching articles from the database
    $sql = "SELECT * FROM articles WHERE itemName LIKE '%$search_query%'";
    $result = $conn->query($sql);

    $articles = array();
    while ($row = $result->fetch_assoc()) {
        $articles[] = $row;
    }

    // Return the search results as JSON
    echo json_encode($articles);
} else {
    echo json_encode(array()); // Return an empty array if no query is provided
}
$conn->close();
?>
