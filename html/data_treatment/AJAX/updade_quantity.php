<?php
session_start();
// Inclure la connexion à la base de données et d'autres configurations nécessaires

if (isset($_POST['itemName']) && isset($_POST['newQuantity'])) {
    $itemName = $_POST['itemName'];
    $newQuantity = intval($_POST['newQuantity']);

    // Mettre à jour la quantité dans la session panier
    $_SESSION['panier'][$itemName]['quantity'] = $newQuantity;

    if($_SESSION['panier'][$itemName]['quantity']==0){
        unset($_SESSION['panier'][$itemName]);
    }

    echo 'Mise à jour de la quantité réussie';
} else {
    // Gérer les erreurs si les paramètres ne sont pas corrects
    echo 'Erreur lors de la mise à jour de la quantité';
}
?>
