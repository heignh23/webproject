<?php
session_start();

// Include PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

// Include your database connection code here
include("connect_database.php");

// Process the AJAX request
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $items = json_encode($_POST['items']);
    $shipmentMethod = $_POST['shipmentMethod'];
    $promocode = $_POST['promoCode'];
    $username = $_SESSION['username'];

    // Insert order details into the "order" table
    // Adjust the SQL query based on your table structure
    $sql = "INSERT INTO `orders` (buyer, items, shipment, promocode) 
            VALUES ('$username', '$items', '$shipmentMethod', '$promocode')";
    $conn->query($sql);

    $sql = "SELECT orderNumber FROM `orders` WHERE buyer='$username' AND items='$items' AND shipment='$shipmentMethod' AND promocode='$promocode' order by orderNumber desc limit 1";
    
    $result = $conn->query($sql);

    $final = $result->fetch_array();

    // Préparer la requête SQL
    $sql = "SELECT discount FROM coupons WHERE couponCode = ?";

    // Préparer et exécuter la déclaration
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $promocode);
    $stmt->execute();
    $stmt->bind_result($result);

    // Vérifier si le code promo existe dans la base de données
    if ($stmt->fetch()) {
        // Renvoyer la remise associée
        $discount = $result;
    } else {
        // Si le code promo n'est pas trouvé, renvoyer 0
        $discount = "0";
    }

    $decoded_items = json_decode($items, true); // Set the second parameter to true for associative arrays
    $totalprice = 0;
    $text = "";
    foreach ($decoded_items as $item) {
        // Decode Unicode escape sequences within the itemName
        $itemName = $item['itemName'];
        $quantityOrdered = $item['quantity'];

        // // Adjust the SQL query based on your table structure
        // $updateQuantitySQL = "UPDATE articles SET quantity = quantity - ? WHERE itemName = ?";
        // $stmt = $conn->prepare($updateQuantitySQL);
        // $stmt->bind_param("is", $quantityOrdered, $itemName);
        // $stmt->execute();
        // $stmt->close();
        $item['itemName'] = json_decode('"' . $item['itemName'] . '"');
        $text .= "- " . $item['quantity'] . " " . $item['itemName'] . " which costs ";
        if ($item['quantity'] >= 16) {
            $text .= $item['price'] * $item['quantity'] * (1 - 0.16) . " € (you have received an extra 16% discount !)<br>";
            $totalprice += $item['price'] * $item['quantity'] * (1 - 0.16);
        } else if ($item['quantity'] >= 8) {
            $text .= $item['price'] * $item['quantity'] * (1 - 0.08) . " € (you have received an extra 8% discount !)<br>";
            $totalprice += $item['price'] * $item['quantity'] * (1 - 0.08);
        } else {
            $text .= $item['price'] * $item['quantity'] * (1) . " €<br>";
            $totalprice += $item['price'] * $item['quantity'];
        }
    }

    if($shipmentMethod	== "DHL Express"){
        $totalprice +=44;
    } else if($shipmentMethod	== "DPD"){
        $totalprice -=19;
    }

    $mailhtml = '<h1>Thank you for your order!</h1><br>Order number:'.$final[0].'<br>Items: <br>'.$text.'You used the '.$shipmentMethod.' Shipment method.<br>';
    if($discount != 0){
        $mailhtml.='You have used the promocode "'.$promocode.'" that reduced the total cost to '.$totalprice*(1-$discount).' €.<br>';
    } else {
        $mailhtml.='The total cost is '.$totalprice.' €.<br>';
    }

    // Send confirmation email
    try {
        $mail = new PHPMailer(true);

        $mail->isSMTP();
        $mail->Host = '127.0.0.1';
        $mail->SMTPAuth = false;
        $mail->Port = 25;

        $mail->setFrom('webshop@localhost.com');
        $mail->addAddress($username);

        $mail->CharSet = 'UTF-8';

        $mail->Subject = 'Order Confirmation';
        $mail->isHTML(true);
        $mail->Body = $mailhtml;

        $mail->send();

        // Reset the shopping cart in the session
        $_SESSION['panier'] = array();

        echo 'Order successful! Check your email for confirmation.';
    } catch (Exception $e) {
        echo "Error sending email: {$mail->ErrorInfo}";
    }

    // Close the database connection
    $conn->close();
}
?>
