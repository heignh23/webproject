<?php
session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include("connect_database.php");

    // Check if username is entered and meets the criteria
    if(isset($_POST["password"]) && strlen($_POST["password"]) >= 9 && $_POST["password"] == $_POST["Cpassword"]) {
        $password = $_POST["password"];

        // Check if password meets complexity requirements
        if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/", $password)) {
            // Password is valid, generate hash
            $hashedPassword = hash("sha512",$password);

            // Now you can use $hashedPassword for further processing or storage
            // For example, you can send it to the server
        } else {
            echo "Invalid password format. Password must be at least nine characters long and contain one upper case letter, one lower case letter, and one number.";
            exit;
        }
    }

    // Requête SQL de recherche
    $sql = "UPDATE members
    SET mdp = '".$hashedPassword."',
        FirstConnection = '0'
    WHERE username = '".$_SESSION["username"]."'";


    // Exécute la requête
    $result = $conn->query($sql);

    if($result){
        echo "Password changed ! Redirecting ...";
        exit;
    } else {
        echo "Error during sql request.";
        exit;
    }
}
?>