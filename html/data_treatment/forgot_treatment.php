<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';
session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include("connect_database.php");
    
    function generateRandomPassword($length = 9) {
        // Caractères possibles dans le mot de passe
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    
        $password = '';
        $charLength = strlen($characters) - 1;
    
        // Génère le mot de passe
        for ($i = 0; $i < $length; $i++) {
            $password .= $characters[mt_rand(0, $charLength)];
        }
    
        return $password;
    }
    
    // Exemple d'utilisation avec un mot de passe de 12 caractères
    $randomPassword = generateRandomPassword(12);

    // Check if username is entered and meets the criteria
    if(isset($_POST["login"]) && strlen($_POST["login"]) >= 5 && strpos($_POST["login"], "@")) {
        $username = $_POST["login"];
        
        // Continue with other checks
    } else {
        echo "Invalid username format. Please enter a valid email address with at least five characters.";
        // You might want to redirect or handle this differently based on your requirements
        exit;
    }

    // Requête SQL de recherche
    $sql = "SELECT username FROM members WHERE username = '".$username."'";

    // Exécute la requête
    $result = $conn->query($sql);

    // Vérifie si des résultats ont été trouvés
    if ($result->num_rows <= 0) {
        // Affiche les résultats
        echo "This email correspond to no account.";
        exit;
    }

    $password = generateRandomPassword();

    // Validate the user credentials (you should use prepared statements to prevent SQL injection)
    $sql = "UPDATE members SET `mdp` = '" . hash("sha512", $password) . "', `FirstConnection` = '1' WHERE `username` = '" . $username . "'";

    $result = $conn->query($sql);

    if ($result) {
    // create a new object
    $mail = new PHPMailer(true);

try {
    $mail->isSMTP();
    $mail->Host = '127.0.0.1';
    $mail->SMTPAuth = false;
    $mail->Port = 25;

    $mail->setFrom('webshop@localhost.com');
    $mail->addAddress($username);

    $mail->Subject = 'Forgot Password ?';

    $mail->isHTML(true);

    $mail->Body = "We got you covered, here's the new temporary password: ".$password."<br>Log in again with this password.";

    $mail->send();
    echo 'A new temporary password has been sent to your Email.';
} catch (Exception $e) {
    echo "Erreur lors de l'envoi de l'email : {$mail->ErrorInfo}";
}
        exit;
    }

    $conn->close();
}
?>