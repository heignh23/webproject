<?php
session_start();
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include("connect_database.php");

    // Check if username is entered and meets the criteria
    if(isset($_POST["login"]) && strlen($_POST["login"]) >= 5 && strpos($_POST["login"], "@")) {
        $username = $_POST["login"];
        
        // Continue with other checks
    } else {
        echo "Invalid username format. Please enter a valid email address with at least five characters.";
        // You might want to redirect or handle this differently based on your requirements
        exit;
    }

    // Check if password is entered
    if(isset($_POST["password"]) && strlen($_POST["password"]) >= 9) {
        $password = $_POST["password"];

        // Check if password meets complexity requirements
        if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/", $password)) {
            // Password is valid, generate hash
            $hashedPassword = hash("sha512",$password);

            // Now you can use $hashedPassword for further processing or storage
            // For example, you can send it to the server
        } else {
            echo "Invalid password format. Password must be at least nine characters long and contain one upper case letter, one lower case letter, and one number.";
            exit;
        }
    } else {
        echo "Password is required or invalid.";
        exit;
    }
    // Validate the user credentials (you should use prepared statements to prevent SQL injection)
    $sql = "SELECT * FROM members WHERE username = '$username' AND mdp = '$hashedPassword'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($rows=$result->fetch_assoc()){
            if($rows["FirstConnection"]==0){
                // Login successful
                echo "Login successful! Redirecting...";
                // Obtiens la largeur et la hauteur de l'écran
                $dateActuelle = date("Y-m-d H:i:s");
                $screen_resolution=$_POST["resolution"];
                $userAgent = $_SERVER['HTTP_USER_AGENT'];
                $sql = "select MAX(`dateOfOccurrence`) as 'dateOfOccurence' from logs where username='".$username."';";
                $result = $conn->query($sql);
                // Vérifie si la requête a réussi
                if ($result !== false) {
                    // Utilise fetch_assoc() seulement si la requête a réussi
                    $row = $result->fetch_assoc();
                    $_SESSION["last_session"] = $row['dateOfOccurence'];
                } else {
                    // Gère l'erreur si la requête échoue
                    $_SESSION["last_session"] = "Erreur de récupération de la dernière session";
                }
                $sql = "INSERT INTO logs (`username`, `changestate`, `dateOfOccurrence`, `screen_resolution`, `OS`) VALUES ('".$username."', 'CON', '".$dateActuelle."', '".$screen_resolution."', '".$userAgent."')";
                $result = $conn->query($sql);
                $_SESSION["username"] = $username;
                exit;
            } else {
                // Login successful
                echo "Login successful! As it is your first login, you'll have to change the password. Redirecting ...";
                // Obtiens la largeur et la hauteur de l'écran
                $dateActuelle = date("Y-m-d H:i:s");
                $screen_resolution=$_POST["resolution"];
                $userAgent = $_SERVER['HTTP_USER_AGENT'];
                $sql = "select MAX(`dateOfOccurrence`) as 'dateOfOccurence' from logs where username='".$username."';";
                $result = $conn->query($sql);
                // Vérifie si la requête a réussi
                if ($result !== false) {
                    // Utilise fetch_assoc() seulement si la requête a réussi
                    $row = $result->fetch_assoc();
                    $_SESSION["last_session"] = $row['dateOfOccurence'];
                } else {
                    // Gère l'erreur si la requête échoue
                    $_SESSION["last_session"] = "Erreur de récupération de la dernière session";
                }
                $sql = "INSERT INTO logs (`username`, `changestate`, `dateOfOccurrence`, `screen_resolution`, `OS`) VALUES ('".$username."', 'CON', '".$dateActuelle."', '".$screen_resolution."', '".$userAgent."')";
                $result = $conn->query($sql);
                $_SESSION["username"] = $username;
                exit;
            }
        }
    } else {
        // Invalid credentials
        echo "Invalid username or password";
        exit;
    }
}
?>