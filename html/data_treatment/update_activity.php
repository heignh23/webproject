<?php
// update_activity.php - Update user's last activity timestamp with cleanup

include 'connect_database.php';


if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];

    $sql = "SELECT * FROM user_sessions WHERE user_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();

    if ($result->num_rows > 0) {
        // User already exists, update last activity timestamp
        $update_sql = "UPDATE user_sessions SET last_activity = CURRENT_TIMESTAMP WHERE user_id = ?";
        $update_stmt = $conn->prepare($update_sql);
        $update_stmt->bind_param("s", $username);
        $update_stmt->execute();
        $update_stmt->close();
    } else {
        // User doesn't exist, insert a new record
        $insert_sql = "INSERT IGNORE INTO user_sessions (user_id) VALUES (?)";
        $insert_stmt = $conn->prepare($insert_sql);
        $insert_stmt->bind_param("s", $username);
        $insert_stmt->execute();
        $insert_stmt->close();
    }
}
?>
