<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot your password?</title>
    <link rel="stylesheet" href="styles.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>

    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <!-- <div class="fadeIn first">
                <img src="https://e1.pxfuel.com/desktop-wallpaper/130/54/desktop-wallpaper-png-pepsi-man-png-pepsiman.jpg" id="icon" alt="User Icon" />
            </div> -->

            <!-- Login Form -->
            
            <p><h3><strong> Forgot your password? </strong></h3></p>
            <p> Don't worry, we got you covered </p>
            <input type="text" id="login" class="fadeIn second" name="login" placeholder="Email">
            <p id="searchResults" style="display:none;"></p>
            <input type="submit" id="submit" class="fadeIn fourth" value="Send Password">
            

            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover" href="login.php">Go back to login</a>
            </div>

        </div>
    </div>
</body>
<script>
    $(document).ready(function(){
        $(document).ready(function(){
        var resolution=screen.width+"x"+screen.height+"";
        $("#submit").on("click", function(){
            // Récupère la valeur de recherche
            var login = $("#login").val();
            // Vérifie si la longueur de la chaîne de recherche est supérieure à 2 caractères
            // Effectue une requête AJAX
            $.ajax({
                type: "POST",
                url: "data_treatment/forgot_treatment.php",
                data: { login: login },
                success: function(response){
                    // Affiche les résultats dans la div #searchResults
                    if(response=="A new temporary password has been sent to your Email."){
                        setTimeout(function() {
                            window.location.href = "login.php";
                        }, 1000);
                    }
                    $("#searchResults").html(response);
                    $("#searchResults").show();
                }
            });
        });
    });
    });
</script>
</html>