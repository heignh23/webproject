<!DOCTYPE html>
<?php if(isset($_SESSION["username"])){
    header("location: index.php");
    exit;
} ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="styles.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <!-- <div class="fadeIn first">
                <img src="https://e1.pxfuel.com/desktop-wallpaper/130/54/desktop-wallpaper-png-pepsi-man-png-pepsiman.jpg" id="icon" alt="User Icon" />
            </div> -->

            <!-- Login Form -->
            
                <p><h3><strong> Log in </strong></h3></p>
                <p> Find the best offers made just for you</p>
                <input type="text" id="login" class="fadeIn second" name="login" placeholder="Email" required>
                <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password" required>
                <p id="result" style="display:none;"></p>
                <input id="submit" type="submit" class="fadeIn fourth" value="Log In">
            

            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover" href="forgot.php">Forgot Password?</a><br>
                New to the WebShop? <a class="underlineHover" href="register.php">Join Now!</a> <br>
                <br> or <a class="underlineHover" href="../startbootstrap-agency-gh-pages/index.php">See offers without login in</a>
            </div>

        </div>
    </div>
</body>
<script>
    $(document).ready(function(){
        var resolution=screen.width+"x"+screen.height+"";
        $("#submit").on("click", function(){
            // Récupère la valeur de recherche
            var login = $("#login").val();
            var password = $("#password").val();
            // Vérifie si la longueur de la chaîne de recherche est supérieure à 2 caractères
            // Effectue une requête AJAX
            $.ajax({
                type: "POST",
                url: "data_treatment/login_treatment.php",
                data: { login: login,password:password,resolution:resolution },
                success: function(response){
                    // Affiche les résultats dans la div #searchResults
                    if(response=="Login successful! Redirecting..."){
                        setTimeout(function() {
                            window.location.href = "../startbootstrap-agency-gh-pages/index.php";
                        }, 1000);
                    } else if(response=="Login successful! As it is your first login, you'll have to change the password. Redirecting ..."){
                        setTimeout(function() {
                            window.location.href = "change_password.php";
                        }, 1000);
                    }
                    $("#result").html(response);
                    $("#result").show();
                }
            });
        });
    });
    </script>
</html>