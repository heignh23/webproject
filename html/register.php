<!DOCTYPE html>
<?php 
session_start();
if(isset($_SESSION["username"])){
    header("location: index.php");
    exit;
} ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="styles.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>

    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <!-- <div class="fadeIn first">
                <img src="https://e1.pxfuel.com/desktop-wallpaper/130/54/desktop-wallpaper-png-pepsi-man-png-pepsiman.jpg" id="icon" alt="User Icon" />
            </div> -->

            <!-- Register Form -->
            
            <p><h3><strong> Register </strong></h3></p>
            <p> Find the best offers made just for you</p>
            <input type="text" id="login" class="fadeIn second" name="login" placeholder="Email" required>
            <p id="searchResults" style="display:none;"></p>
            <input type="submit" id="submit" class="fadeIn third" value="Join">
            

            <!-- Remind Passowrd -->
            <div id="formFooter">

                Already registered? <a class="underlineHover" href="login.php">Log In</a>
            </div>

        </div>
    </div>
</body>
<script>
    $(document).ready(function(){  
        $("#login").on("input", function(){
            // Récupère la valeur de recherche
            var login = $("#login").val();
            // Vérifie si la longueur de la chaîne de recherche est supérieure à 2 caractères
            // Effectue une requête AJAX
            $.ajax({
                type: "POST",
                url: "data_treatment/AJAX/check_username_register.php",
                data: { login: login },
                success: function(response){
                    // Affiche les résultats dans la div #searchResults
                    $("#searchResults").html(response);
                    $("#searchResults").show();
                }
            });
        });
        

        var resolution=screen.width+"x"+screen.height+"";
        $("#submit").on("click", function(){
            // Récupère la valeur de recherche
            var login = $("#login").val();
            // Vérifie si la longueur de la chaîne de recherche est supérieure à 2 caractères
            // Effectue une requête AJAX
            $.ajax({
                type: "POST",
                url: "data_treatment/register_treatment.php",
                data: { login: login },
                success: function(response){
                    // Affiche les résultats dans la div #searchResults
                    if(response=="Registration successful! Check your email for confirmation."){
                        setTimeout(function() {
                            window.location.href = "login.php";
                        }, 1000);
                    }
                    $("#searchResults").html(response);
                    $("#searchResults").show();
                }
            });
        });
    });

</script>
</html>