<?php 
session_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Checkout example · Bootstrap v5.0</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/checkout/">

    

    <!-- Bootstrap core CSS -->
<link href="/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
<link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
<meta name="theme-color" content="#7952b3">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    
<div class="container">
  <main>
    <div class="py-5 text-center">
      <h2>YOUR ORDER</h2>
      <img src="assets/img/XD.jpeg" alt="" width="150" height="150"><br><br>
    </div>

    You have ordered:<br>
<?php
$text="";
foreach($_SESSION["panier"] as $item){
    $text .="- ".$item['quantity']." ".$item["itemName"]." which costs ";
    if($item['quantity']>=16){
        $text .= $item["price"]*$item["quantity"]*(1-0.16)." € (you have received a 16% discount !)<br>";
    } else if($item['quantity']>=8){
        $text .= $item["price"]*$item["quantity"]*(1-0.08)." € (you have received a 8% discount !)<br>";
    } else {
        $text .= $item["price"]*$item["quantity"]*(1)." €<br>";
    }
}
echo $text;
?>
<br><br><br>

    <div class="row g-5">
      <div class="col-md-5 col-lg-4 order-md-last">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-primary">Your cart</span>
          <span class="badge bg-primary rounded-pill">3</span>
        </h4>
        <ul class="list-group mb-3">
        <?php $totalprice = 0;
        foreach($_SESSION['panier'] as $item){?>
          <li class="list-group-item d-flex justify-content-between lh-sm">
            <div>
              <h6 class="my-0"><?php echo $item['itemName']?></h6>
              <small class="text-muted"><?php echo 'Quantity: '.$item["quantity"].' item.'?></small>
            </div>
            <span class="text-muted"><?php if($item['quantity']>=16){
                    echo $item["price"]*$item["quantity"]*(1-0.16)." € (16% discount)";
                    $totalprice += $item["price"]*$item["quantity"]*(1-0.16);
                } else if($item['quantity']>=8){
                    echo $item["price"]*$item["quantity"]*(1-0.08)." € (8% discount)";
                    $totalprice += $item["price"]*$item["quantity"]*(1-0.08);
                } else {
                    echo $item["price"]*$item["quantity"]." €";
                    $totalprice += $item["price"]*$item["quantity"];
                }?>
            </span>
          </li>
          <?php }?>
          <li class="list-group-item d-flex justify-content-between bg-light">
            <div class="text-success">
              <h6 class="my-0">Promo code</h6>
              <small id = "idCodeshow"></small>
            </div>
            <span class="text-success"></span>
          </li>
          <li class="list-group-item d-flex justify-content-between">
            <span>Total (EURO)</span>
            <strong><?php echo $totalprice;?> €</strong>
          </li>
        </ul>

        <form class="card p-2">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Promo code" id="promocode" <?php if(isset($_SESSION["promocode"])){echo 'value="'.$_SESSION["promocode"].'"';}?>>
          
        </div>
        </form>
      </div>
      <div class="col-md-7 col-lg-8">
        <h4 class="mb-3">Billing address</h4>
        <form class="needs-validation" novalidate>
          <div class="row g-3">
            <div class="col-sm-6">
              <label for="firstName" class="form-label">First name</label>
              <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid first name is required.
              </div>
            </div>

            <div class="col-sm-6">
              <label for="lastName" class="form-label">Last name</label>
              <input type="text" class="form-control" id="lastName" placeholder="" value="" required>
              <div class="invalid-feedback">
                Valid last name is required.
              </div>
            </div>

            <div class="col-12">
              <label for="address" class="form-label">Address</label>
              <input type="text" class="form-control" id="address" placeholder="1234 Main St" required>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="col-12">
              <label for="address2" class="form-label">Address 2 <span class="text-muted">(Optional)</span></label>
              <input type="text" class="form-control" id="address2" placeholder="Apartment or suite">
            </div>

            <div class="col-md-5">
              <label for="country" class="form-label">Country</label>
              <select class="form-select" id="country" required>
                <option value="">Choose...</option>
                <option>United States</option>
              </select>
              <div class="invalid-feedback">
                Please select a valid country.
              </div>
            </div>

            <div class="col-md-4">
              <label for="state" class="form-label">State</label>
              <select class="form-select" id="state" required>
                <option value="">Choose...</option>
                <option>California</option>
              </select>
              <div class="invalid-feedback">
                Please provide a valid state.
              </div>
            </div>

            <div class="col-md-3">
              <label for="zip" class="form-label">Zip</label>
              <input type="text" class="form-control" id="zip" placeholder="" required>
              <div class="invalid-feedback">
                Zip code required.
              </div>
            </div>
          </div>

          <h4 class="mb-3">Shipping Method</h4>

          <div class="my-3">
            <div class="form-check">
              <input id="DHL Express" name="shipmentMethod" type="radio" class="form-check-input" required <?php if(isset($_SESSION["shipment"]) && $_SESSION["shipment"] === "DHL Express") {echo "checked";}?>>
              <label class="form-check-label" for="DHL Express">DHL Express (+44 €)</label>
            </div>
            <div class="form-check">
              <input id="DHL" name="shipmentMethod" type="radio" class="form-check-input" required <?php if(isset($_SESSION["shipment"]) && $_SESSION["shipment"] === "DHL") {echo "checked";}?>>
              <label class="form-check-label" for="DHL">DHL</label>
            </div>
            <div class="form-check">
              <input id="DPD" name="shipmentMethod" type="radio" class="form-check-input" required <?php if(isset($_SESSION["shipment"]) && $_SESSION["shipment"] === "DPD") {echo "checked";}?>>
              <label class="form-check-label" for="DPD">DPD (-19 €)</label>
            </div>
          </div>

          <hr class="my-4">

          <div class="form-check">
            <input type="checkbox" class="form-check-input" id="DataProtection" required>
            <label class="form-check-label" for="same-address">By using this website you are agreeing to our Privacy Policy and our Data Protection Policy</label>
            </div>

          <hr class="my-4">

          <h4 class="mb-3">Payment</h4>

          <div class="my-3">
            <div class="form-check">
              <input id="credit" name="paymentMethod" type="radio" class="form-check-input" checked required>
              <label class="form-check-label" for="credit">Credit card</label>
            </div>
            <div class="form-check">
              <input id="debit" name="paymentMethod" type="radio" class="form-check-input" required>
              <label class="form-check-label" for="debit">Debit card</label>
            </div>
            <div class="form-check">
              <input id="paypal" name="paymentMethod" type="radio" class="form-check-input" required>
              <label class="form-check-label" for="paypal">PayPal</label>
            </div>
          </div>

          <div class="row gy-3">
            <div class="col-md-6">
              <label for="cc-name" class="form-label">Name on card</label>
              <input type="text" class="form-control" id="cc-name" placeholder="" required>
              <small class="text-muted">Full name as displayed on card</small>
              <div class="invalid-feedback">
                Name on card is required
              </div>
            </div>

            <div class="col-md-6">
              <label for="cc-number" class="form-label">Credit card number</label>
              <input type="text" class="form-control" id="cc-number" placeholder="" required>
              <div class="invalid-feedback">
                Credit card number is required
              </div>
            </div>

            <div class="col-md-3">
              <label for="cc-expiration" class="form-label">Expiration</label>
              <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
              <div class="invalid-feedback">
                Expiration date required
              </div>
            </div>

            <div class="col-md-3">
              <label for="cc-cvv" class="form-label">CVV</label>
              <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
              <div class="invalid-feedback">
                Security code required
              </div>
            </div>
          </div>

          <hr class="my-4">

          <button class="w-100 btn btn-primary btn-lg" id="endcheckout">Continue to checkout</button>
        </form>
      </div>
    </div>
  </main>

  <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; 2017–2021 Company Name</p>
    <ul class="list-inline">
      <li class="list-inline-item"><a href="#">Privacy</a></li>
      <li class="list-inline-item"><a href="#">Terms</a></li>
      <li class="list-inline-item"><a href="#">Support</a></li>
    </ul>
  </footer>
</div>


<script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="js/slideBox.js"></script>
      <script src="form-validation.js"></script>
  </body>
</html>
<?php echo json_encode($_SESSION["panier"]); ?>
<!-- Add this script to the end of your HTML file, before the closing </body> tag -->
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
  $(document).ready(function() {
    // Function to update total price based on shipment method
    function updateTotalPrice() {
      // Get the selected shipment method
      var selectedMethod = $("input[name='shipmentMethod']:checked").attr("id");

      // Récupérer le code promo saisi par l'utilisateur
      var promoCode = $("#promocode").val();

      // Get the current total price
      var totalPrice = <?php echo $totalprice; ?>;

      // Update total price based on the selected method
      switch (selectedMethod) {
        case "DHL Express":
          totalPrice += 44; // Add 44 to the total
          break;
        case "DPD":
          totalPrice -= 19; // Subtract 19 from the total
          break;
        // Add more cases for other shipment methods if needed

        default:
          break;
      }
      
      // Faire une requête AJAX pour obtenir la remise associée au code promo
      $.ajax({
        url: '../html/data_treatment/AJAX/check_coupon.php', // Remplacez 'get_discount.php' par le chemin réel de votre script serveur
        type: 'POST',
        data: { promoCode: promoCode },
        success: function(response) {
          // Mettre à jour le prix total avec la remise obtenue
          var discount = parseFloat(response);
          console.log(response);
          totalPrice = totalPrice - (totalPrice * discount); // Appliquer la remise
          console.log(totalPrice);
          var discountInfo = "";
          if (discount) {
            discountInfo += "Promo Code Applied: " + promoCode;
            discountInfo += " - Discount: " + (discount * 100) + "%";
          }
          // Update the displayed total price
          $("strong").text(totalPrice.toFixed(2) + " €");

           // Update the discount information in the small tag
          $("#idCodeshow").text(discountInfo);

        
        },
      });
    }

    // Attacher l'événement de changement à l'input du code promo
    $("#promocode").on('input', function() {
      updateTotalPrice();
    });

    // Attach change event to shipment method radio buttons
    $("input[name='shipmentMethod']").change(function() {
      updateTotalPrice();
    });

    // Initial update when the page loads
    updateTotalPrice();

    // Click event for checkout button
    $("#endcheckout").on("click", function() {
      event.preventDefault();

      if ($("#DataProtection").prop("checked")) {

      // Gather data for the AJAX request
      var items = <?php echo json_encode($_SESSION["panier"]); ?>;
      var shipmentMethod = $("input[name='shipmentMethod']:checked").attr("id");
      var promoCode = $("#promocode").val();
      var totalPrice = <?php echo $totalprice; ?>;

      // Prepare data to be sent
      var data = {
        items: items,
        shipmentMethod: shipmentMethod,
        promoCode: promoCode,
        totalPrice: totalPrice
      };

      // Make the AJAX request
      $.ajax({
        url: '../html/data_treatment/AJAXendcheckout.php', // Replace with the actual path
        type: 'POST',
        data: data,
        success: function(response) {
          // Redirect to the thank you page upon successful response
          window.location.href = 'thank_you_page.php';
        },
        error: function() {
          // Handle error if needed
        }
      });
      } else {
        // Display an alert if DataProtection checkbox is not checked
        alert("Please check the Data Protection checkbox before proceeding.");
      }
    });
  });
</script>