<!DOCTYPE html>
<?php session_start();
if(isset($_SESSION["last_session"])){
include("../html/data_treatment/update_activity.php");

    list($year, $month, $day) = explode('-', explode(' ', $_SESSION["last_session"])[0]);

// Calcul de c
$c = ($month <= 2) ? 1 : 0;

// Calcul de a
$a = $year - $c;

// Calcul de m
$m = $month + 12 * $c - 2;

// Calcul de j
$j = ($day + $a + floor($a/4) - floor($a/100) + floor($a/400) + floor((31*$m)/12)) % 7;
switch ($j) {
    case 0:
        $Fday = "Sunday";
        break;
    case 1:
        $Fday = "Monday";
        break;
    case 2:
        $Fday = "Tuesday";
        break;
    case 3:
        $Fday = "Wednesday";
        break;
    case 4:
        $Fday = "Thursday";
        break;
    case 5:
        $Fday = "Friday";
        break;
    case 6:
        $Fday = "Saturday";
        break;
}
}
include("../html/data_treatment/connect_database.php");
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Web Shop</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/883fc94f2e.js" crossorigin="anonymous"></script>
        <!-- JQuery -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    <script>


function updateOnlineUsers() {
$.ajax({
    url: '../html/data_treatment/AJAX/online_Users.php',
    success: function (data) {
        $('#onlineUsers').text('Online Users: ' + data);
    }
});
}

// Update online user count every 30 seconds
setInterval(updateOnlineUsers, 30000);

// Initial update
updateOnlineUsers();
function addToCart(button) {
var itemId = button.getAttribute("data-item-id");
var itemName = button.getAttribute("data-item-name");
var itemPrice = button.getAttribute("data-item-price");
var itemQuantity = document.getElementById("quantity" + itemId).value;
// Faire une requête Ajax pour ajouter l'article au panier
$.ajax({
url: '../html/data_treatment/AJAX/modifier_panier.php', // Correction du chemin du fichier
method: 'POST',
data: {
    itemName: itemName,
    itemPrice: itemPrice,
    itemQuantity: itemQuantity
},
dataType: 'json',
success: function (response) {
    alert(response.message); // Vous pouvez personnaliser cela en fonction de votre logique
    $('#numberbox').text(parseInt($('#numberbox').text(), 10)+parseInt(itemQuantity),10);
},
error: function (error) {
    console.error(error);
}
});
}

</script>

    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                <a href="shoppingPage.php"><i class="fa-solid fa-cart-shopping"></i></a>
                <div class="number-box" id="numberbox">
                    <?php 
                    $count = 0;
                    if(isset($_SESSION["panier"])){
                        foreach($_SESSION["panier"] as $item){
                            $count +=intval($item["quantity"]);
                        }
                    }
                    echo $count;?>
                </div>
                <style>
                    .number-box {
                        width: 30px;
                        height: 30px;
                        background-color: #f0f0f0;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        font-size: 24px;
                        font-weight: bold;
                        margin-right: 240px;
                    }
                </style>
                <div id="onlineUsers" style="color:white">Loading ...</div>
                
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        
                    <li>
                        </li>

                        <li>
                        <form method="GET">
                            <input type="text" name="query" id=search placeholder="Search...">
                        </form>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#services">Offers</a></li>
                        <li class="nav-item"><a class="nav-link" href="#portfolio">Laptops</a></li>
                        <li class="nav-item"><a class="nav-link" href="purchased_item.php">My Orders</a></li>
                        <?php if(isset($_SESSION["username"])){?>
                            <li class="nav-item"><a class="nav-link" href="../html/data_treatment/logout_treatment.php">Log Out</a></li>
                        <?php } else {?>
                            <li class="nav-item"><a class="nav-link" href="../html/login.php">Log In</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <style>
                #search {
                    width: 50%;
                    margin-right: 50px ;
                }
                .fa-cart-shopping{
                    color: white;
                    font-size: 25px;
                    margin-right: 10px;
                        }
            </style>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="containerM">
                <div class="masthead-subheading">Welcome
                    <?php if(isset($_session["username"])){echo "".explode('@',$_SESSION["username"])[0]."! You were last online on ".$Fday." - ".$day.".".$month.".".$year.".";}
                    ?>
                </div>
                <div class="masthead-heading text-uppercase">Find some good offers now!</div>
                <a class="btn btn-primary btn-xl text-uppercase" href="#services">Tell Me More</a>
            </div>
        </header>
        <!-- Services-->
        <section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Offers</h2>
                    <h3 class="section-subheading text-muted">Some offers you may like</h3>
                </div>

                <!-- Slideshow container -->
                <div class="slideshow-container">
                    

                    <div class="mySlides fade">
                    <img src="assets/img/laptops/1.jpg" style="width:50%">
                    ASUS ROG Strix G15 - $1,100
                    </div>
 
                
                    <div class="mySlides fade">
                    <img src="assets/img/laptops/2.jpg" style="width:50%">
                    HP OMEN 16 - $950 
                    </div>
                
                    <div class="mySlides fade">
                    <img src="assets/img/laptops/3.jpg" style="width:50%">
                    Acer Nitro 5 - $1,150 
                    </div>
                
                    <!-- Next and previous buttons -->
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>
                </div>
                <br>
                
                <!-- The dots/circles -->
                <div style="text-align:center">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                </div>
                
                <br>

            </div>
        </section>

    <?php 
 
    $sql = "SELECT * FROM articles WHERE quantity > 0";

    $result = $conn->query($sql);
    $articles = array();
    while ($row = $result->fetch_assoc()) {
        $articles[] = $row;
    }
    ?>

        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Laptops</h2>
                    <h3 class="section-subheading text-muted">The best quality is here</h3>
                </div>
                <div class="row">
                    <?php
                    for($i=0; $i<count($articles); $i++){ 
                    ?>
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 1-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" <?php echo 'href="#portfolioModal'.$i.'"' ?>>
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src=<?php echo "'../image/laptops/".$articles[$i]["imagename"]."'"; ?> alt="..." />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading"><?php echo $articles[$i]["itemName"]?></div>
                                <div class="portfolio-caption-subheading text-muted"><?php echo $articles[$i]["price"]." €"?></div>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    <?php };?>
                </div>
            </div>
        </section>
        
    
        
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Your Website 2023</div>
                    <div class="col-lg-4 my-3 my-lg-0">
                        <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Twitter"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="Facebook"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!" aria-label="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <div class="col-lg-4 text-lg-end">
                        <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                        <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Portfolio Modals-->
        <!-- Portfolio item 1 modal popup-->
        <?php
            for($i=0; $i<count($articles); $i++){ 
        ?>
        <div class="portfolio-modal modal fade" <?php echo 'id="portfolioModal'.$i.'"' ?> tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body" <?php echo 'id="modal'.$articles[$i]["itemName"].'"';?>>
                                    <!-- Project details-->
                                    <h2 class="text-uppercase"><?php echo $articles[$i]["itemName"]?></h2>
                                    <!-- <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p> -->
                                    <img class="img-fluid d-block mx-auto" src=<?php echo "'../image/laptops/".$articles[$i]["imagename"]."'"; ?> alt="..." />
                                    <p><?php echo $articles[$i]["description"]; ?></p>
                                    <p><?php echo "Price: ".$articles[$i]["price"]." € <br>Quantity available: ".$articles[$i]["quantity"];?></p>
                                    <input type="hidden" id=hiddenquantity value=<?php echo $articles[$i]["quantity"];?>>
                                    <?php if(isset($_SESSION["username"])){?>
                                    <div class="quantity">
                                        <button onclick="decrementQuantity(this)">-</button>
                                        <input type="text" value="1" <?php echo 'id="quantity'.$i.'"'; ?>>
                                        <button onclick="incrementQuantity(this)">+</button>
                                    </div>
                                    <button class="btn btn-primary btn-xl text-uppercase" type="button" 
                                            data-item-id="<?php echo $i; ?>"
                                            data-item-name="<?php echo $articles[$i]["itemName"]; ?>"
                                            data-item-price="<?php echo $articles[$i]["price"]; ?>"
                                            onclick="addToCart(this)">
                                            Add to shopping cart
                                        </button>
                                    <?php } ?>
                                        <script>
                                            function incrementQuantity(button) {
                                                var input = button.parentNode.querySelector('input');
                                                var quantity = parseInt(button.parentNode.parentNode.querySelector('input[type="hidden"]').value,10);
                                                var value = parseInt(input.value, 10);
                                                if(value+1<= quantity){
                                                input.value = value + 1;
                                                }
                                            }

                                            function decrementQuantity(button) {
                                                var input = button.parentNode.querySelector('input');
                                                var value = parseInt(input.value, 10);
                                                if (value > 0) {
                                                    input.value = value - 1;
                                                }
                                            }
                                            function removeProduct(button) {
                                                var product = button.parentNode.parentNode;
                                                product.parentNode.removeChild(product);
                                        }
                                        </script>
                                        <style>
                                            .button-container {
                                            text-align: center;
                                        }
                                            .product {
                                            border: 1px solid #ddd;
                                            margin: 1em;
                                            padding: 1em;
                                            text-align: center;
                                        }
                                            .page-section h3.section-subheading, .page-section .section-subheading.h3 {
                                            margin-bottom: 0;
                                        }
                                            .remove-button {
                                            background-color: #ff6961; /* Light red color */
                                            color: #fff;
                                            border: none;
                                            padding: 0.5em;
                                            cursor: pointer;
                                        }
                                            .quantity button {
                                            background-color: #333;
                                            color: #fff;
                                            border: none;
                                            padding: 0.5em;
                                            cursor: pointer;
                                        }

                                            .quantity input {
                                            width: 2em;
                                            text-align: center;
                                            margin: 0 0.5em;
                                        }
                                        
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }; ?>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="js/slideBox.js"></script>
    </body>
</html>
