<?php 
session_start();
include("../html/data_treatment/update_activity.php");
if(!isset($_SESSION["username"])){
    header("location: ../html/login.php");
    exit;
} 
?>
<button type="button" onclick="redirectToPage()">Return</button> <br><br><br>

<script>
  function redirectToPage() {
    window.location.href = "index.php";
  }
</script>

<?php
// Include your database connection code here
include("../html/data_treatment/connect_database.php");

// Fetch all orders for a specific buyer
$buyerEmail = $_SESSION["username"]; // Replace with the actual buyer's email

$sql = "SELECT * FROM orders WHERE buyer = '$buyerEmail' ORDER BY orderNumber DESC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Display the summary for each order
    while ($row = $result->fetch_assoc()) {
        echo '<h2>Order Number: ' . $row['orderNumber'] . '</h2>';
        
        // Decode the JSON-encoded items
        $items = json_decode($row['items'], JSON_UNESCAPED_UNICODE);
        

        echo '<ul>';
        // Display each item in the order
        foreach ($items as $itemName => $itemDetails) {
            echo '<li>';
            echo 'Item: ' . $itemDetails['itemName'] . '<br>';
            echo 'Price: ' . $itemDetails['price'] . ' €<br>';
            echo 'Quantity: ' . $itemDetails['quantity'] . '<br>';
            echo '<img src="../image/laptops/' . $itemDetails['imagename'] . '" alt="Image" height="87" >' . '<br>';
            echo '</li><br>';
        }
        echo '</ul>';

        echo '<p>Shipment Method: ' . $row['shipment'] . '</p>';
        echo '<p>Promocode Used: ' . $row['promocode'] . '</p>';

        // Add "Buy Again" and "Buy Again Instantly" buttons

        echo '<input type="hidden" name="orderNumber" value="' . $row['orderNumber'] . '">';
        echo '<button class="buy-again-btn" data-order-number="' . $row['orderNumber'] . '">Buy Again</button>';

        echo '<hr>';
    }
} else {
    echo '<p>No orders found for this buyer.</p>';
}

// Close the database connection
$conn->close();
?>
<link href="css/styles.css" rel="stylesheet" />
<!-- JQuery -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
    // Function to handle "Buy Again" button click
    $(".buy-again-btn").click(function() {
      // Get the orderNumber from the data attribute
      var orderNumber = $(this).data("order-number");

      // Make an AJAX request to buy_again.php
      $.ajax({
        url: '../html/data_treatment/AJAX/buy_again.php',
        type: 'POST',
        data: { orderNumber: orderNumber },
        success: function(response) {
          // Redirect to checkout.php upon success
          window.location.href = 'checkout.php';
        },
        error: function() {
          // Handle error if needed
        }
      });
    });
  });
</script>
<style>
  /* styles.css */

body {
    font-family: 'Arial', sans-serif;
    background-color: #f4f4f4;
    margin: 20px;
    text-align: center;
}

h2 {
    color: #333;
}

ul {
    list-style-type: none;
    padding: 0;
}

li {
    background-color: #fff;
    border: 1px solid #ddd;
    margin-bottom: 10px;
    padding: 10px;
}

p {
    margin: 10px 0;
}

.buy-again-btn {
    background-color: #4CAF50;
    color: #fff;
    padding: 10px;
    border: none;
    cursor: pointer;
}

.buy-again-btn:hover {
    background-color: #45a049;
}

hr {
    border: 1px solid #ddd;
}
</style>
