<?php 
session_start();
include("../html/data_treatment/update_activity.php");
if(!isset($_SESSION["username"])){
    header("location: ../html/login.php");
    exit;
} 
// Récupérez les noms d'articles depuis le panier
$itemNames = array_keys($_SESSION["panier"]);

// Construisez une chaîne de noms d'articles formatée pour la requête SQL
$itemListCsv = "'" . implode("','", $itemNames) . "'";

// Exécutez la requête SQL pour récupérer les informations sur les articles du panier
$sql = "SELECT * FROM articles WHERE itemName IN ($itemListCsv)";
$result = $conn->query($sql);

// Traitement des résultats ici...
while ($row = $result->fetch_assoc()) {
    // Accédez aux données de chaque article
    $_SESSION["panier"][$row["itemName"]]["imagename"] = $row["imagename"];
    $_SESSION["panier"][$row["itemName"]]["itemName"] = $row["itemName"];
    ;


    // Faites ce que vous avez besoin de faire avec ces informations
    // Par exemple, affichez-les ou les utilisez d'une autre manière
}
?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Web Shop</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/883fc94f2e.js" crossorigin="anonymous"></script>
        <!-- JQuery -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                <a href="index.php"><i class="fa-solid fa-chevron-left"></i></a>
                    <style>
                        .fa-chevron-left{
                            color: white;
                            font-size: 25px
                        }
                    </style>
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            
                        <li class="nav-item"><a class="nav-link" href="../html/data_treatment/logout_treatment.php">Log Out</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- product Grid-->
        <section class="page-section bg-light">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Your cart</h2>
                    <h3 class="section-subheading text-muted">These are the items you are about to buy</h3>
                </div>
            </div>
        </section>
        <?php foreach($_SESSION["panier"] as $item){
            $sql = "SELECT * FROM articles WHERE quantity > 0 AND itemName = '" . $item["itemName"] . "'";


            $result = $conn->query($sql);
            $articles = array();
            while ($row = $result->fetch_assoc()) {
                $articles[] = $row;
            }
             ?>
            <div class="product">
                <img <?php echo 'src="../image/laptops/'.$item['imagename'].'"'?> style="width:10%">
                <h2><?php echo $item["itemName"];?></h2>
                <input type="hidden" value="<?php echo $item["price"];?>" id=unitprice-<?php echo $item["itemName"];?>>
                <input type="hidden" id=hiddenquantity value=<?php echo $articles[0]["quantity"];?>>
                <p id="price-<?php echo $item["itemName"]; ?>" data-price="<?php echo $item["price"]; ?>">
                <?php if($item['quantity']>=16){
                    echo "Price: ".$item["price"]*$item["quantity"]*(1-0.16)." € (16% discount)";
                } else if($item['quantity']>=8){
                    echo "Price: ".$item["price"]*$item["quantity"]*(1-0.08)." € (8% discount)";
                } else {
                    echo "Price: ".$item["price"]*$item["quantity"]." €";
                }?></p>
                <div class="quantity">
                    <button class="remove-button" onclick="removeProduct(this, '<?php echo $item["itemName"]; ?>')">X</button>
                    <button onclick="decrementQuantity(this, '<?php echo $item["itemName"]; ?>')">-</button>
                    <input type="text" value=<?php echo '"'.$item["quantity"].'" id="quantity-'.$item["itemName"].'"';?> readonly>
                    <button onclick="incrementQuantity(this, '<?php echo $item["itemName"]; ?>')">+</button>
                </div>
            </div>
        <?php }?>


        <div class="button-container">
        <a class="btn btn-primary btn-xl text-uppercase" href="checkout.php" style="background-color: #ff6961; color: #fff; border: none; padding: 10px 20px; text-decoration: none; text-align: center; display: inline-block; font-size: 16px;">Checkout</a>
        </div>

        <script>
            function updateQuantity(itemName, newQuantity) {
                $.ajax({
                    type: 'POST',
                    url: '../html/data_treatment/AJAX/updade_quantity.php', // Crée un fichier PHP pour traiter la mise à jour de la quantité
                    data: { itemName: itemName, newQuantity: newQuantity },
                    success: function(response) {
                    },
                    error: function(error) {
                        console.error('Erreur lors de la mise à jour de la quantité:', error);
                    }
                });
            }

            function incrementQuantity(button, itemName) {
                var input = button.parentNode.querySelector('input');
                var value = parseInt(input.value, 10);
                var maxquantity = parseInt(button.parentNode.parentNode.querySelector('input[type="hidden"]:nth-of-type(2)').value,10);
                console.log(maxquantity);
                if(value+1<= maxquantity){
                    input.value = value + 1;
                }
                updateQuantity(itemName, input.value);
                var price = button.parentNode.parentNode.querySelector('p');
                var unitprice = button.parentNode.parentNode.querySelector('input[type="hidden"]');
                

                if(input.value>=16){
                    price.innerText = "Price: "+parseInt(unitprice.value,10)*input.value*(1-0.16)+" € (16% discount)";
                } else if(input.value>=8){
                    price.innerText = "Price: "+parseInt(unitprice.value,10)*input.value*(1-0.08)+" € (8% discount)";
                } else {
                    price.innerText = "Price: "+parseInt(unitprice.value,10)*input.value+" €";
                }
            }

            function decrementQuantity(button, itemName) {
                var input = button.parentNode.querySelector('input');
                var value = parseInt(input.value, 10);
                if (value > 0) {
                    input.value = value - 1;
                    var price = button.parentNode.parentNode.querySelector('p');
                var unitprice = button.parentNode.parentNode.querySelector('input[type="hidden"]');
                    if(input.value>=16){
                        price.innerText = "Price: "+parseInt(unitprice.value,10)*input.value*(1-0.16)+" € (16% discount)";
                    } else if(input.value>=8){
                        price.innerText = "Price: "+parseInt(unitprice.value,10)*input.value*(1-0.08)+" € (8% discount)";
                    } else {
                        price.innerText = "Price: "+parseInt(unitprice.value,10)*input.value+" €";
                    }
                    updateQuantity(itemName, input.value);
                    if(input.value == 0){
                        var product = button.parentNode.parentNode;
                        product.parentNode.removeChild(product);
                    }
                }
            }

            function removeProduct(button, itemName) {
                var product = button.parentNode.parentNode;
                product.parentNode.removeChild(product);
                updateQuantity(itemName, 0); // 0 signifie retirer complètement l'article
            }
        </script>
        <style>
            .button-container {
            text-align: center;
        }
            .product {
            border: 1px solid #ddd;
            margin: 1em;
            padding: 1em;
            text-align: center;
        }
            .page-section h3.section-subheading, .page-section .section-subheading.h3 {
            margin-bottom: 0;
        }
            .remove-button {
            background-color: #ff6961; /* Light red color */
            color: #fff;
            border: none;
            padding: 0.5em;
            cursor: pointer;
        }
            .quantity button {
            background-color: #333;
            color: #fff;
            border: none;
            padding: 0.5em;
            cursor: pointer;
        }

            .quantity input {
            width: 2em;
            text-align: center;
            margin: 0 0.5em;
        }
        
        </style>

        

        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Your Website 2023</div>

                    <div class="col-lg-4 text-lg-end">
                        <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                        <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                    </div>
                </div>
            </div>
        </footer>
        
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/staticNavBar.js"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
        <script src="js/slideBox.js"></script>
    </body>
</html>
