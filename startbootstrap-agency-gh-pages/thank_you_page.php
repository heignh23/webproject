<?php
session_start();
include("../html/data_treatment/update_activity.php");
if(!isset($_SESSION["username"])){
    header("location: ../html/login.php");
    exit;
}  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="../html/styles.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <div class="wrapper fadeInDown">
        <div id="formContent">
            
                <p><h3><strong> Thanks for your purchase! </strong></h3></p>
                <p> You will soon receive an email with information. </p>
            
            <div id="formFooter">
                <a href="index.php">Return to Main Page</a>
            </div>

        </div>
    </div>
</body>
</html>