-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2024 at 04:53 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `itemName` text NOT NULL,
  `price` int(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `couponCode` text NOT NULL,
  `discount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `changestate` enum('CON','DIS') DEFAULT NULL,
  `dateOfOccurrence` datetime NOT NULL,
  `screen_resolution` varchar(255) DEFAULT NULL,
  `OS` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `username`, `changestate`, `dateOfOccurrence`, `screen_resolution`, `OS`) VALUES
(1, 'admin@admin.admin', 'CON', '2024-01-06 00:00:00', '1x1', 'windows'),
(9, 'admin@admin.admin', 'CON', '2024-01-07 21:14:14', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(8, 'admin@admin.admin', 'CON', '2024-01-07 21:10:17', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(7, 'admin@admin.admin', 'CON', '2024-01-06 23:31:49', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(10, 'admin@admin.admin', 'CON', '2024-01-07 21:14:15', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(11, 'admin@admin.admin', 'CON', '2024-01-07 21:14:18', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(12, 'admin@admin.admin', 'CON', '2024-01-07 21:15:33', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(13, 'admin@admin.admin', 'CON', '2024-01-07 21:15:35', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(14, 'admin@admin.admin', 'CON', '2024-01-07 21:16:21', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(15, 'admin@admin.admin', 'CON', '2024-01-07 21:17:23', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(16, 'admin@admin.admin', 'CON', '2024-01-07 21:18:04', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(17, 'admin@admin.admin', 'CON', '2024-01-07 21:20:08', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(18, 'admin@admin.admin', 'CON', '2024-01-07 21:38:44', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(19, 'admin@admin.admin', 'CON', '2024-01-07 21:49:29', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(20, 'admin@admin.admin', 'CON', '2024-01-07 21:56:23', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(21, 'admin@admin.admin', 'CON', '2024-01-07 22:44:36', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(22, 'admin@admin.admin', 'CON', '2024-01-07 22:49:28', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(23, 'admin@admin.admin', 'CON', '2024-01-07 22:50:48', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(24, 'admin@admin.admin', 'CON', '2024-01-07 22:51:07', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(25, 'admin@admin.admin', 'CON', '2024-01-07 22:53:28', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(26, 'admin@admin.admin', 'CON', '2024-01-07 22:53:46', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(27, 'admin@admin.admin', 'CON', '2024-01-07 22:53:58', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(28, 'charlie@localhost.com', 'CON', '2024-01-08 03:05:26', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(29, 'charlie@localhost.com', 'CON', '2024-01-08 03:06:22', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(30, 'charlie@localhost.com', 'CON', '2024-01-08 03:06:46', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(31, 'charlie@localhost.com', 'CON', '2024-01-08 03:10:34', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(32, 'charlie@localhost.com', 'CON', '2024-01-08 03:14:46', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(33, 'charlie@localhost.com', 'CON', '2024-01-08 03:19:21', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(34, 'admin@admin.admin', 'CON', '2024-01-09 16:36:37', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(35, 'charlie@localhost.com', 'CON', '2024-01-09 16:38:45', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb'),
(36, 'charlie@localhost.com', 'CON', '2024-01-09 16:39:41', '1920x1080', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `username` text NOT NULL,
  `mdp` text NOT NULL,
  `FirstConnection` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`username`, `mdp`, `FirstConnection`) VALUES
('admin@admin.admin', '035dd3fe67892133d5ed4d759d0d203808a18988d9f3e5e3f31e097380e335a0571da6a3be9bc8893e0b4eaabc580f05865406d8cc1c0794671f96ff0d69c823', 0),
('charlie@localhost.com', '6d5e65e3674e15812d8c02963f9d2788ec0a5490919eeac2652ec3e47f4d22fa35cea2f4a09834686163157b9a803832dc944bbc876686f3b3e72bac1aac74c8', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderNumber` text NOT NULL,
  `buyer` text NOT NULL,
  `itemName` text NOT NULL,
  `price` int(6) NOT NULL,
  `quantity` int(6) NOT NULL,
  `shipment` enum('DPD','DHL','DHL Express') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`itemName`(191));

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`couponCode`(191));

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`(250));

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`username`(191));

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderNumber`(191)),
  ADD KEY `itemName` (`itemName`(250)),
  ADD KEY `buyer` (`buyer`(250));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
